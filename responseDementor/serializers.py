from rest_framework import serializers
from .models import PortofolioData,WhatAmIDoing,BookReview

class PortofolioDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = PortofolioData
        fields = ("title","category","short_description","description","image","numberOrder","url")

class WhatAmIDoingSerializer(serializers.ModelSerializer):
    class Meta:
        model = WhatAmIDoing
        fields = ("time","update")

class BookReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = BookReview
        fields = ("status","title","finishDate","url","notes")