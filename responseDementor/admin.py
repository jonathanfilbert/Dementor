from django.contrib import admin
from .models import PortofolioData,WhatAmIDoing,BookReview

# Register your models here.
admin.site.register(PortofolioData)
admin.site.register(WhatAmIDoing)
admin.site.register(BookReview)
