from django.db import models
import datetime


# Create your models here.
CATEGORY_CHOICES = [("Design", "Design"), ("Development", "Development"), ("Social",
                                                                           "Social"), ("Leadership", "Leadership")]


class PortofolioData(models.Model):
    numberOrder = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    category = models.CharField(max_length=100, choices=CATEGORY_CHOICES)
    short_description = models.CharField(max_length=50)
    description = models.TextField()
    image = models.TextField(max_length=1000)
    url = models.TextField()


class WhatAmIDoing(models.Model):
    time = models.DateField(default=datetime.date.today)
    update = models.CharField(max_length=200)

class BookReview(models.Model):
    status = models.CharField(max_length=10)
    title = models.CharField(max_length=100)
    finishDate = models.DateField(default=datetime.date.today)
    url = models.TextField()
    notes = models.TextField()
